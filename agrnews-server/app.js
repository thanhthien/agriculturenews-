
const express = require("express"),
  app = express(),
  exphbs = require("express-handlebars"),
  morgan = require("morgan"),
  createError = require("http-errors"),
  categoriesRoutes = require("./routes/categories"),
  postsRoutes = require("./routes/posts"),
  usersRoutes = require("./routes/users"),
  bodyParser = require("body-parser");
  var path = require('path');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(require("./middlewares/category.mdw"));
app.use(morgan("dev"));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});
app.use(express.static('public'));

// app.use(require("./middlewares/category.mdw"));\
require('./middlewares/passport')(app);

const directory = path.join(__dirname, '/public');
app.use('/public', express.static(directory));

app.use("/posts", postsRoutes);
app.use("/users", usersRoutes);
app.use("/categories", categoriesRoutes);
app.use('/static', express.static("../agrnews/public/images/"));


app.get("/test", function (req, res) {
  res.render("test");
});

app.get("/", function (req, res) {
  res.render("home");
});


app.get("/error", function (req, res) {
  res.render("error", {
    layout: false
  });
});


app.use((req, res, next) => {
  next(createError(404));
});

app.use((err, req, res, next) => {

  var status = err.status || 500;
  var vwErr = "error";
  if (status === 404) {
    vwErr = "404";
  }

  process.env.NODE_ENV = process.env.NODE_ENV || 'dev';
  var isProd = process.env.NODE_ENV === 'prod';
  var message = isProd ? 'An error has occured. Please contact administartor for more support.' : err.message;
  var error = isProd ? {} : err;

  var message = isProd ? 'An error has occured. Please contact administartor for more support.' : err.message;
  var error = isProd ? {} : err;

  res.status(status).render(vwErr, {
    layout: false,
    message,
    error
  });
})

var port = 8000;
app.listen(port, () => {
  console.log(`This server is running at port ${port}`);
});
