var express = require("express"),
  router = express.Router(),
  user_model = require("../models/user.model"),
  user_mdw = require("../middlewares/nodemailer"),
  passport = require('passport'),
  bcrypt = require('bcrypt');

router.post("/register", (req, res) => {
  console.log(req.body)
  var saltRounds = 10,
    hash = bcrypt.hashSync(req.body.pass, saltRounds),
    entity = req.body;
  delete entity.pass;
  entity.Password = hash;
  entity.UserRights = "Subscriber";
  delete entity.confirm;
  user_model.add(entity)
    .then(results => {
      console.log(results)
      return res.status(200).send(results)
    })
    .catch(error => {
      console.log(error)
      return res.status(500).send(error)
    });
});

router.post("/available", (req, res) => {
  console.log(req.body);
  const { name, email } = req.body;
  user_model.single(name, email).then(rows => {
    console.log(rows);
    if (rows.length > 0)
      res.send("false");
    else res.send("true");
  })
})


router.post('/login', (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    if (err)
      return next(err);
    if (!user)
      return res.send(null);
    else {
      delete user.Password;
      return res.send(user)

    }
  })(req, res, next);

})

router.post('/changepass', (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.send(info.message);
    }
    else {
      var saltRounds = 10,
        hash = bcrypt.hashSync(req.body.newPasword, saltRounds);
      delete req.body.password;
      delete req.body.newPasword;
      var entity = {};
      entity["Id"] = req.body.id;
      entity["Username"] = req.body.username;
      entity["Password"] = hash;
      user_model.updatePassword(entity)
        .then(results => {
          return res.status(200).send("success")
        })
        .catch(error => {
          return res.status(500).send(error)
        });
    }
  })(req, res, next);
})

router.post('/nodemailer', (req, res, next) => {
  var Email = req.body.Email;
  user_model.singleByEmail(Email)
    .then(results => {
      var saltRounds = 10,
        pass = '041019',
        Password = bcrypt.hashSync(pass, saltRounds),
        entity = {};
      entity["Id"] = results[0].Id;
      entity["Password"] = Password;
      user_model.updatePassword(entity)
        .then(results => {
          user_mdw.userTransporter(Email)
          return res.status(200).send("success")
        })
        .catch(error => {
          res.status(200).send("fail")
        });
    })
    .catch(error => {
      res.status(200).send("fail")
    });
})

router.post("/admin/all", (req, res) => {
  user_model.listUserByAdmin()
    .then(rows => {
      return res.status(200).send(rows)
    })
    .catch(error => {
      return res.status(500).send(error)
    });
});


router.post("/admin/user", (req, res) => {
  user_model.updateUseByAdmin(req.body)
    .then(results => {
      return res.status(200).send("success");
    })
    .catch(error => {
      return res.status(500).send("fail");
    });
});

router.post("/admin/user/delete", (req, res) => {
  var Id = req.body.Id;
  console.log(req.body);
  user_model.deleteUseByAdmin(Id)
    .then(results => {
      return res.status(200).send("success");
    })
    .catch(error => {
      return res.status(500).send("fail");
    });
});

module.exports = router;  
