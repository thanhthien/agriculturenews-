var express = require("express"),
  router = express.Router(),
  posts_model = require("../models/post.model"),
  UploadImage = require("../middlewares/upload"),
  path = require('path'),
  fs = require('fs'),
  request = require('request'),
  multer = require('multer'),
  cors = require('cors');
var moment = require('moment');



router.get("/", (req, res) => {
  posts_model
    .allWithDetails()
    .then(rows => {
      return res.status(200).json(rows)
    })
    .catch(error => {
      res.status(500).send(error)
    });
});
router.get("/10news", (req, res) => {
  posts_model
    .allTop10News()
    .then(rows => {
      return res.status(200).json(rows)
    })
    .catch(error => {
      res.status(500).send(error)
    });
});


router.get("/10views", (req, res) => {
  posts_model
    .allTop10Views()
    .then(rows => {
      return res.status(200).json(rows)
    })
    .catch(error => {
      res.status(500).send(error)
    });
});

router.post("/sameCat", (req, res) => {
  posts_model
    .allSameCategory(req.body.PostID, req.body.CatID)
    .then(rows => {
      return res.status(200).json(rows)
    })
    .catch(error => {
      res.status(500).send(error)
    });
});



router.post("/get", (req, res) => {
  posts_model
    .allByCat(req.body.url)
    .then(rows => {
      return res.status(200).json(rows)
    })
    .catch(error => {
      res.status(500).send(error)
    });
});

router.post("/update/views", (req, res) => {
  var entity = req.body;
  posts_model
    .updateViews(entity)
    .then(result => {
      return res.status(200).json("success")
    })
    .catch(error => {
      res.status(500).send(error)
    });
});



router.post("/getpost", (req, res) => {
  console.log(req.body)
  posts_model
    .detailPost(req.body.Url)
    .then(result => {
      return res.status(200).json(result)
    })
    .catch(error => {
      res.status(500).send(error)
    });
});



router.post("/page", (req, res) => {
  posts_model
    .getAllWithDetails(req.body.url, req.body.pageIndex, req.body.pageSize)
    .then(rows => {
      return res.status(200).json(rows)
    })
    .catch(error => {
      res.status(500).send(error)
    });
});

router.post("/search", (req, res) => {
  posts_model
    .getAllWithSearch(req.body.Search)
    .then(rows => {
      return res.status(200).json(rows)
    })
    .catch(error => {
      res.status(500).send(error)
    });
});



router.post("/uploadImage", function (req, res) {
  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname)
    }
  });
  var upload = multer({ storage: storage }).single('file');

  upload(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      return res.status(500).json(err)
    } else if (err) {
      return res.status(500).json(err)
    }
    return res.status(200).send(req.file.filename)

  })

});



router.post("/add", (req, res) => {
  var entity = req.body;
  var now = new Date();
  entity['createdAt'] = moment(now).format("YYYY-MM-DD");
  entity.ShortName = req.body.FullName;
  entity.Status = "Not yet approved";
  posts_model.add(entity)
    .then(results => {
      return res.status(200).send(results)
    })
    .catch(error => {
      return res.status(500).send(error)
    });
});

router.post("/list", (req, res) => {
  var UserID = req.body;
  posts_model.listPostWriter(UserID)
    .then(rows => {
      console.log(rows)
      return res.status(200).send(rows)
    })
    .catch(error => {
      return res.status(500).send(error)
    });
});

router.post("/editor/list", (req, res) => {
  var UserID = req.body;
  posts_model.listPostEditor(UserID)
    .then(rows => {
      return res.status(200).send(rows)
    })
    .catch(error => {
      return res.status(500).send(error)
    });
});

router.post("/admin/list", (req, res) => {
  var UserID = req.body;
  posts_model.listPostAdmin()
    .then(rows => {
      return res.status(200).send(rows)
    })
    .catch(error => {
      return res.status(500).send(error)
    });
});




router.post("/editor/all", (req, res) => {
  var PostID = req.body;
  posts_model.AllPostEditor(PostID)
    .then(rows => {
      return res.status(200).send(rows)
    })
    .catch(error => {
      return res.status(500).send(error)
    });
});

router.post("/edit/status", (req, res) => {
  console.log("aaaaaaaaaa");
  var PostID = req.body.PostID,
    entity = {};
    entity.Status = req.body.Status;
  console.log(PostID, entity);
  posts_model.UpdateStatusPost(PostID, entity)
    .then(rows => {
      return res.status(200).send("success")
    })
    .catch(error => {
      return res.status(200).send("fail")

    });
});

module.exports = router;  
