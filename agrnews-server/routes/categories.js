var express = require("express");
var router = express.Router();
var category_model = require("../models/category.model");


router.get("/", (req, res) => {
  category_model.all()
    .then(rows => {
      return res.status(200).json(list_to_tree(rows))
    })
    .catch(error => {
      res.status(500).send(error)
    });
})

router.post("/addcat", (req, res) => {
  console.log(req.body);
  var entity = req.body;
  if (!entity.Url)
    delete entity.Url
  if (!entity.ParentIDS)
    delete entity.ParentIDS
  category_model.addCat(entity)
    .then(results => {
      return res.status(200).send("success");
    })
    .catch(error => {
      res.status(200).send.send("faill");
    });
})


router.get("/admin/all", (req, res) => {
  category_model
    .allByAdmin()
    .then(rows => {
      return res.status(200).json(list_to_tree(rows))
    })
    .catch(error => {
      res.status(500).send(error)
    });
})

router.post("/admin/cat", (req, res) => {
  var entity = req.body;
  category_model
    .update(entity)
    .then(results => {
      return res.status(200).send("success");
    })
    .catch(error => {
      return res.status(500).send("fail");
    });
})


router.post("/admin/delete", (req, res) => {
  var CatID = req.body.CatID;
  category_model.SelectParentIDS(CatID)
    .then(rows => {
      if (rows.length > 0) {
        category_model
          .deleteChild(rows).then(results => {
            category_model
              .delete(CatID)
              .then(results => {
                return res.status(200).send("success");
              })
              .catch(error => {
                return res.status(500).send("fail");
              });
          })
      } else {
        category_model
          .delete(CatID)
          .then(results => {
            return res.status(200).send("success");
          })
          .catch(error => {
            return res.status(500).send("fail");
          });
      }
    })
    .catch(error => {
      return res.status(500).send("fail");
    });
})


function list_to_tree(list) {
  var map = {}, node, roots = [], i;
  for (i = 0; i < list.length; i += 1) {
    map[list[i].CatID] = i; // initialize the map
    list[i].children = []; // initialize the children
  }
  for (i = 0; i < list.length; i += 1) {
    node = list[i];
    if (node.ParentIDS !== 0) {
      // if you have dangling branches check that map[node.parentId] exists

      list[map[node.ParentIDS]].children.push(node);
    } else {
      roots.push(node);
    }
  }
  return roots;
}

module.exports = router;
