var db = require("../utils/db");

module.exports = {
  all: () => {
    return db.load("select * from Users");
  },

  add: entity => {
    console.log(entity)
    return db.add("Users", entity);
  },

  single: (userName, email) => {
    console.log(userName, email)
    return db.load(`select * from Users where Username like "${userName}" or Email like "${email}"`);
  },

  singleByEmail: (email) => {
    console.log(email)
    return db.load(`select * from Users where Email like "${email}"`);
  },

  singleByUserName: userName => {
    return db.load(`select * from Users where Username = '${userName}'`);
  },

  listUserByAdmin: () => {
    return db.load(`select Id, Username, Email, Dob, UserRights from Users`);
  },

  updatePassword: entity => {
    var id = entity.Id;
    delete entity.Id;
    return db.update("Users", "Id", entity, id);
  },

  updateUseByAdmin: entity => {
    var id = entity.Id;
    delete entity.Id;
    console.log(entity)
    return db.update("Users", "Id", entity, id);
  },
  
  deleteUseByAdmin: entity => {
    var Id = entity;
    return db.delete("Users", "Id", Id);
  }
  
}