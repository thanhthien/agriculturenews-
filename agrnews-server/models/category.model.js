var db = require("../utils/db");

module.exports = {
  all: () => {
    return db.load("select * from Categories");
  },

  addCat: entity => {
    console.log(entity)
    return db.add("Categories", entity);
  },
  
  
  allByAdmin: () => {
    return db.load("select * from Categories");
  },

  SelectParentIDS: (Id) => {
    var id = Id;
    return db.load(`select c2.CatID from Categories c1, Categories c2 where c1.CatID = ${id} and c1.CatID = c2.ParentIDS`);
  },

  add: entity => {
    console.log(entity);
    return db.add("Categories", entity);
  },

  update: entity => {
    var id = entity.CatID;
    delete entity.CatID;
    return db.update("Categories", "CatID", entity, id);
  },


  allWithDetails: () => {
    return db.load(
      "SELECT C.*, count(P.proID) as num_of_products FROM Categories C  left join products P on P.CatID = C.CatID group by C.CatID, C.CatName"
    );
  },

  single: id => {
    return db.load(`select * from Categories where CatID = ${id}`);
  },

  delete: entity => {
    var id = entity;  
    return db.delete("Categories", "CatID", id);
  },

  deleteChild: entity => {
    var a = [];
    entity.forEach(element => {
      a.push(element.CatID)
    });
    console.log(a)
    return db.deleteMultiple(`DELETE FROM Categories Where CatID IN (${a})`);
  },
};
