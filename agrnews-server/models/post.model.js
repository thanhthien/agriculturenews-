var db = require("../utils/db");

module.exports = {
  allWithDetails: () => {
    return db.load("SELECT * from Posts where Status Like 'Approved' ORDER BY createdAt DESC LIMIT 4");
  },

  allTop10News: () => {
    return db.load("SELECT * from Posts where Status Like 'Approved' ORDER BY createdAt DESC LIMIT 10");
  },

  allTop10Views: () => {
    return db.load("SELECT * from Posts where Status Like 'Approved' ORDER BY views DESC LIMIT 10");
  },

  allSameCategory: (PostID, CatID) => {
    console.log(PostID, CatID);
    return db.load(`SELECT * from Posts where Status Like 'Approved' and CatID = ${CatID} and PostID != ${PostID} LIMIT 5`);
  },
  

  add: entity => {
    return db.add("Posts", entity);
  },

  detailPost: (Url) => {
    return db.load(`Select * from Posts where Url like "${Url}"`);
  },
  

  listPostWriter: entity => {
    var Id = entity.UserID;
    var query = `select p.PostID,  p.Url, p.FullName, p.ShortDescription, 
      p.FullDescription, p.Status from Posts p , 
      Users u where p.UserID = u.Id and p.UserID = ${Id}`;
    return db.load(query);
  },

  listPostEditor: entity => {
    var Id = entity.UserID;
    var query = `SELECT u2.Id ,p.PostID, p.Url, p.FullName, 
        p.Status from Users u , Users u2, Posts p where u.Id = u2.ManageID
        and u.Id = ${Id} and u2.Id = p.UserID and p.Status not like 'Approved'` ;
    return db.load(query);
  },

  listPostAdmin:()=>{
    var query = `SELECT u2.Id ,p.PostID, p.Url, p.FullName, 
    p.Status from Users u2, Posts p where u2.Id = p.UserID and p.Status not like 'Approved'`;
    return db.load(query);
  },

  UpdateStatusPost: (PostID, entity) => {
    console.log(PostID, entity)
    return db.update("Posts", "PostID", entity, PostID);
  },

  updateViews: (entity) => {
    var PostID = entity.PostID;
    delete entity.PostID;
    return db.update("Posts", "PostID", entity, PostID);
  },

  AllPostEditor: entity => {
    var PostID = entity.PostID;
    var query = `Select * from Posts where PostID = ${PostID}` ;
    return db.load(query);
  },

  getAllWithSearch: Search => {
    var query = `Select * from Posts where FullName like '%${Search}%'
        or  ShortDescription like '%${Search}%'
        or  FullDescription like '%${Search}%' and Status Like 'Approved'`;
    return db.load(query);
  },
  
  

  allByCat: url => {
    var query = `select c2.CatName as NameParent,
    c.CatName, p.PostID, p.Url, p.FullName, p.ShortDescription,
    p.createdAt, p. views, p.avatar 
         from Posts p , Categories c, Categories c2
         where c.ParentIDS = c2.CatID and c.CatID = p.CatID and c.Url = "${url}"
          and p.Status Like 'Approved'`;
    return db.load(query);
  },

  
  getAllWithDetails: (url, pageIndex, pageSize) => {
    var pagenumber = pageIndex * pageSize;
    var query = `select c2.CatName as NameParent,
    c.CatName, p.PostID, p.Url, p.FullName, p.ShortDescription,
    p.createdAt, p. views, p.avatar 
         from Posts p , Categories c, Categories c2
         where  c.ParentIDS = c2.CatID and c.CatID = p.CatID 
         and p.Status Like 'Approved' and c.Url = "${url}"`;
    return db.load(query);
  },

};

