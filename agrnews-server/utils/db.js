var mysql = require("mysql");

var createConnection = () =>
  mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "agrnews",
    port: 3306
  });

module.exports = {
  load: sql => {
    return new Promise((resolve, reject) => {
      var connection = createConnection();
      connection.connect();
      connection.query(sql, (error, results, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
        connection.end();
      });
    });
  },

  add: (tableName, entity) => {
    return new Promise((resolve, reject) => {
      var connection = createConnection();
      connection.connect();
      var sql = `Insert into ${tableName} set ?`;
      connection.query(sql, entity, (error, results) => {
        if (error) {
          reject(error);
        } else {  
          resolve(results);
        }
        connection.end();
      });
    });
  },

  deleteMultiple: sql => {
    return new Promise((resolve, reject) => {
      var connection = createConnection();
      connection.connect();
      connection.query(sql, (error, results, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(results.affectedRows);
        }
        connection.end();
      });
    });
  },

  delete: (tableName, idField, id) => {
    return new Promise((resolve, reject) => {
      var connection = createConnection();
      var sql = `delete from ${tableName} where ${idField} = ? `;
      connection.connect();
      connection.query(sql, id, (error, results, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(results.affectedRows);
        }
        connection.end();
      });
    });
  },


  update: (tableName, idField, entity, id) => {
    return new Promise((resolve, reject) => {
      var connection = createConnection();
      var sql = `update ${tableName} set ? where ${idField} = ?`;
      connection.connect();
      connection.query(sql, [entity, id], (error, results, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
        connection.end();
      });
    });
  }
};


