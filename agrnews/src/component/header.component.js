import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../assets/img/logo.png'
import ad from '../assets/img/ad-2.jpg'
import logo2 from '../assets/img/logo-alt.png'
import categoryServices from '../services/category.services';
import postServices from '../services/post.services';
import viewHelper from '../lib/viewHelper';

class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            login: [],
            name: '',
            user: {},
        }
    }


    componentWillMount() {
        this.setState({
            user: JSON.parse(localStorage.getItem('user')),
        });
        categoryServices.getAll()
            .then(response => response.data)
            .then(data => {
                this.setState({
                    categories: data
                });
            })
            .catch(err => {
                console.log(err);
            });
        postServices.getListPost();
    }



    getCategory = () => {
        let categories = this.state.categories;
        var cate = [];
        console.log(categories)
        cate.push(<li className=""><a href="http://127.0.0.1:3000">Trang chủ</a></li>)
        if (categories.length > 0) {
            categories.forEach(function (item) {
                if (item.children.length > 0) {
                    const child = item.children.map((itemChild, key) =>
                        <li><a href={"/category/" + itemChild.Url} >{itemChild.CatName}</a></li>);
                    cate.push(<li className=""><a href={item.Url}>{item.CatName}</a>
                        <ul>{child}</ul>
                    </li>);
                } else {
                    cate.push(<li className=""><a href={item.Url}>{item.CatName}</a></li>);
                }
            });
        }
        return cate;
    }

    CheckUser = () => {
        var user = this.state.user,
            Users = [];
        if (user.UserRights == "Writer") {
            Users.push(<li><a href="http://127.0.0.1:3000/writer">Post an article (Writer)</a></li>);
            Users.push(<li><a href="http://127.0.0.1:3000/postswriter">article list (Writer)</a></li>);
            return Users
        }
        else if (user.UserRights == "Editor") {
            Users.push(<li><a href="http://127.0.0.1:3000/postseditor">Browse the list (Editor)</a></li>);
            return Users
        }
        else if (user.UserRights == "Admin") {
            Users.push(<li><a href="http://127.0.0.1:3000/admin/category">Category (Admin)</a></li>);
            Users.push(<li><a href="http://127.0.0.1:3000/admin/post">Post (Admin)</a></li>);
            Users.push(<li><a href="http://127.0.0.1:3000/admin/uses">User (Admin)</a></li>);
            return Users
        }

    }

    checkUserLogin = () => {
        var user = this.state.user,
            Users = [];
        if (viewHelper.isEmpty(user)) {
            Users.push(<li>
                <a class="" data-toggle="dropdown"><i class="fa fa-user-circle" aria-hidden="true"> HI {user.Username}</i>
                    <span class="caret"></span></a>
                <ul class=".main-nav">
                    <li><a href="http://127.0.0.1:3000/account">Account</a></li>
                    <li ><a href="http://127.0.0.1:3000/changepassword">Setting Password</a></li>
                    <li><a href="http://127.0.0.1:3000/login">Logout</a></li>
                    {this.CheckUser()}
                </ul>
            </li>
            );
        }
        else {
            Users.push(
                <li><a href="http://127.0.0.1:3000/login"
                ><i className="fa fa-sign-in"></i> Login</a></li>);
        }
        return Users;
    }
    render() {

        return (
            <>
                <header id="header">
                    <div id="top-header">
                        <div className="container">
                            <div className="header-links">
                                <ul>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Contact</a></li>
                                    <li><a href="#">Advertisement</a></li>
                                    <li><a href="#">Privacy</a></li>
                                </ul>
                            </div>
                            <div className="header-social">
                                <ul>
                                    <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i className="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i className="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i className="fa fa-youtube"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div id="center-header">
                        <div className="container">
                            <div className="header-logo">
                                <a href="#" className="logo"><img src={logo} alt="Logo" /></a>
                            </div>
                            <div className="header-ads">
                                <img className="center-block" src={ad} alt="Ad" />
                            </div>
                        </div>
                    </div>
                    <div id="nav-header">
                        <div className="container">
                            <nav id="main-nav">
                                <div className="nav-logo">
                                    <a href="#" className="logo"><img src={logo2} alt="" /></a>
                                </div>
                                <ul className="main-nav nav navbar-nav" >
                                    {this.getCategory()}
                                    {this.checkUserLogin()}
                                </ul>
                            </nav>
                            <div class="button-nav">
                            <button class="search-collapse-btn"><a href ="http://127.0.0.1:3000/search/"><i class="fa fa-search"></i></a></button>
                                <button class="nav-collapse-btn"><i class="fa fa-bars"></i></button>

                            </div>
                        </div>
                    </div>
                </header>
            </>

        );
    }
}
export default Header