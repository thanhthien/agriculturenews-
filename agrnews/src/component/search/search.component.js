import React, { Component } from 'react';
import '../../assets/static_login/css/style.css'
import postServices from '../../services/post.services'
import searchresult from '../../component/search'
import { createBrowserHistory } from 'history';
import queryString from 'query-string';
import { Redirect } from 'react-router-dom'
const history = createBrowserHistory();


class Search extends Component {
    constructor(props) {
        super(props);
        var self = this;
        this.handleSubmit = self.handleSubmit.bind(this);
        this.state = {
            search: '',
            alert: [],
        };
    }

    handleSubmit(event) {
        event.preventDefault();
        // var search = event.target.Search.value;
    }

    render() {
        return (
            <div class="main">
                <section class="signup">
                    <div class="container">
                        <div class="signup-content">
                            <form action="/searchresult" >
                                <h2 class="form-title">Search By Title And Content</h2>
                                <br /><br />
                                {this.state.alert}
                                <div class="form-group">
                                    <input class="form-control" type="search" name="PostName" id="PostName" placeholder="Search" aria-label="Search" />
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="submit" id="submit" class="form-submit" value="Submit" />
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>

        );
    };
}
export default Search;