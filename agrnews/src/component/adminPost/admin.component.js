import React, { Component } from 'react';
import PostService from '../../services/post.services';
import Modal from 'react-modal';
import Select from 'react-select';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
const options = [
    { value: 'Not yet approved', label: 'Not yet approved' },
    { value: 'Rejected', label: 'Rejected' },
    { value: 'Not yet approved', label: 'Not yet approved' },
    { value: 'Approved', label: 'Approved' },
    { value: 'Waiting for publication', label: 'Waiting for publication' },
];

const customStyles = {
    content: {
        top: '70%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};
Modal.setAppElement(document.createElement('div'))
class AdminPost extends React.Component {
    constructor(props) {
        super(props);
        this.loadDataListPost = this.loadDataListPost.bind(this);
        this.state = {
            list: [],
            post: [],
            modalIsOpen: false,
            Status: '',
            PostID: '',

        }
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }


    componentWillMount() {
        this.setState({
            user: JSON.parse(localStorage.getItem('user')),
        });
    }
    componentDidMount() {
        return this.loadDataListPost();
    }

    handleSelect = Status => {
        this.setState({ Status: Status.value });
    };


    UpdateStatus = () => {
        var Status = this.state.Status,
            PostID = this.state.PostID;
        PostService.UpdateStatusPost(PostID, Status).then(res => {
            if (res.data == "success" && this.state.Status == "Waiting for publication") {
                this.loadDataListPost();
                this.setState({ modalIsOpen: false });
                NotificationManager.success('Update status is success and Will publish after 10 days! ', 'Message');
            }
            else if (res.data == "success") {
                this.loadDataListPost();
                this.setState({ modalIsOpen: false });
                NotificationManager.success('Update status is success!', 'Message');
            }
            else
                NotificationManager.warning('Warning message', 'Close after 3000ms', 3000);

        });

    }

    loadDataListPost = () => {
        PostService.loadListPostAdmin().then(res => {
            var listPost = res.data;
            this.setState({
                modalIsOpen: false,
                list: listPost.map((item, i) => (<tr>
                    <td key={i}>{item.Id}</td>
                    <td key={i}>{item.PostID}</td>
                    <td key={i}>{item.Url}</td>
                    <td key={i}>{item.FullName}</td>
                    <td key={i}>{item.Status}</td>
                    <button type="button" class="btn btn-info"
                        onClick={() => this.openModal(item)} ><i class="fa fa-pencil-square-o"></i> Detail</button>
                </tr>))
            });
        });
    }

    openModal(item) {
        this.setState({ modalIsOpen: true });
        var PostID = item.PostID;
        PostService.getAllDataPost(PostID).then(res => {
            if (res.data.length >= 0) {
                console.log(res.data);
                var listPost = res.data;
                this.setState({
                    Status: listPost[0].Status,
                    PostID: listPost[0].PostID,
                    post: listPost.map((item, i) => (<div className="modalArticle">
                        <h4>Url : </h4>
                        <p key={i}>{item.Url}</p>
                        <h4>Title : </h4>
                        <p key={i}>{item.FullName}</p>
                        <h4>Short Description : </h4>
                        <p key={i}>{item.ShortDescription}</p>
                        <h4>Full Description : </h4>
                        <p key={i}>{item.FullDescription}</p>
                        <h4>Create Date : </h4>
                        <p key={i}>{item.createdAt}</p>
                        <h4>Name IMG: </h4>
                        <p key={i}>{item.avatar}</p>
                        <h4>Status : </h4>
                        <Select
                            placeholder={item.Status}
                            onChange={this.handleSelect}
                            options={options}
                            isSearchable={false}
                        /><br />
                    </div >))
                });

            }
        });
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        this.subtitle.style.color = '#5F9EA0';
    }

    closeModal() {
        this.setState({ modalIsOpen: false });
    }

    render() {
        const { Status } = this.state;

        return (
            <postwriter>
                <NotificationContainer />
                <h2>Admin Assigned</h2>
                <table className="listPosts">
                    <tr>
                        <th width="30px">WriterID</th>
                        <th width="30px">PostID</th>
                        <th>Url</th>
                        <th>Srticle Title</th>
                        <th>Status</th>
                        <th width="70px">Detail</th>

                    </tr>
                    {this.state.list}
                </table>
                <div className="modalPostAdmin">
                    <Modal
                        isOpen={this.state.modalIsOpen}
                        onAfterOpen={this.afterOpenModal}
                        onRequestClose={this.closeModal}
                        style={customStyles}
                        contentLabel="Post Modal"
                    >
                        <h2 ref={subtitle => this.subtitle = subtitle}>Detailed Article (Admin)</h2>
                        {this.state.post}
                        <button type="button" class="btn btn-success" onClick={this.UpdateStatus}>update</button>
                        <button type="button" class="btn btn-danger" onClick={this.closeModal}>close</button>

                    </Modal>
                </div>
            </postwriter>

        );
    };
}
export default AdminPost;