import React, { Component } from 'react';
import '../../assets/static_login/css/style.css'
import catServices from '../../services/category.services'
import searchresult from '../../component/search'
import { createBrowserHistory } from 'history';
import queryString from 'query-string';
import { Redirect } from 'react-router-dom'
const history = createBrowserHistory();


class AddCat extends Component {
    constructor(props) {
        super(props);
        var self = this;
        this.handleSubmit = self.handleSubmit.bind(this);
        this.state = {
            search: '',
            alert: [],
        };
    }

    handleSubmit(event) {
        event.preventDefault();
        var CatName = event.target.CatName.value,
            Url = event.target.Url.value,
            ParentIDS = event.target.ParentIDS.value;
        if (!CatName ) {
            alert("Input is not available!");
        }
        else {
            console.log("aaaaaaaaaaaa");
            this.setState({ alert: [] })
            console.log(CatName, Url, ParentIDS);
            catServices.addCat(CatName, Url, ParentIDS).then(data => {
                if (data.data == "success") {
                    var joined = this.state.alert.concat(<div class="alert alert-success">
                        <strong>Success!</strong> Add Category Is Success!
                    </div>);
                    this.setState({ alert: joined })
                    return this.state.alert;
                }
                else {
                    var joined = this.state.alert.concat(<div class="alert alert-danger">
                        <strong>Danger!</strong> Error Add Category!
                    </div>);
                    this.setState({ alert: joined })
                    return this.state.alert;
                }
            });
        }
    }

    render() {
        return (
            <div class="main">
                <section class="signup">
                    <div class="container">
                        <div class="signup-content">
                            <form method="POST" id="loginForm" onSubmit={this.handleSubmit} >
                                <h2 class="form-title">Add Category</h2>
                                <br /><br />
                                {this.state.alert}
                                <div class="form-group">
                                    <input class="form-control" type="text" name="CatName" id="CatName" placeholder="CatName" aria-label="CatName" />
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="Url" id="Url" placeholder="Url" aria-label="Url" />
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="ParentIDS" id="ParentIDS" placeholder="ParentIDS" aria-label="ParentIDS" />
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="submit" id="submit" class="form-submit" value="Submit" />
                                </div>
                            </form>
                            <div class="form-group">
                                <a href="http://127.0.0.1:3000/addCat"><input type="submit" name="submit" id="submit" class="form-submit" value="Back" /></a>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        );
    };
}
export default AddCat;