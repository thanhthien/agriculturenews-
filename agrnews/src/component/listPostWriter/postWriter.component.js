import React, { Component } from 'react';
import PostService from '../../services/post.services'
import axios from 'axios';


class ListPostOfWriter extends React.Component {
    constructor(props) {
        super(props);
        this.loadDataListPost = this.loadDataListPost.bind(this);
        this.state = {
            list: [],
        }
    }

    componentWillMount() {
        this.setState({
            user: JSON.parse(localStorage.getItem('user')),
        });
    }
    componentDidMount() {
        return this.loadDataListPost();
    }


    loadDataListPost = () => {
        let UserID = this.state.user.Id;
        var Pos = [];
        PostService.loadListPost(UserID).then(res => {
            var listPost = res.data;
            this.setState({
                list: listPost.map((item, i) => (<tr>
                    <td key={i}>{item.PostID}</td>
                    <td key={i}>{item.Url}</td>
                    <td key={i}>{item.FullName}</td>
                    <td key={i}>{item.Status}</td>
                    {/* <button type="button" class="btn btn-primary">Primary</button> */}
                </tr>))
            });
        });
    }

    modalPost=(id)=>{
        console.log(id);
    }    

    render() {
        console.log(this.state.user);
        return (
            <postwriter>
                <h2>Your List Posts</h2>
                <table className="listPosts">
                    <tr>
                        <th width="30px">PostID</th>
                        <th>Url</th>
                        <th>Srticle Title</th>
                        <th>Status</th>
                    </tr>
                    {this.state.list}
                </table>

            </postwriter>
        );
    };
}
export default ListPostOfWriter;