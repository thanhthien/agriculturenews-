import React, { Component } from 'react';
import PostService from '../../services/post.services'
import Select from 'react-select';
import axios from 'axios';

const options = [
    { value: 1, label: 'TIN TỨC : NÔNG NGHIỆP VIỆT NAM' },
    { value: 3, label: 'TIN TỨC : NÔNG NGHIỆP THẾ GIỚI' },
    { value: 4, label: 'THỊ TRƯỜNG: THỊ TRƯỜNG CHUNG' },
    { value: 5, label: 'THỊ TRƯỜNG: LÚA GẠO' },
    { value: 6, label: 'THỊ TRƯỜNG: CA PHE' },
    { value: 7, label: 'THỊ TRƯỜNG:CAO SU' },
    { value: 8, label: 'THỊ TRƯỜNG: THUY HAI SAN' },
    { value: 9, label: 'Du Lieu Du BAO: NÔNG NGHIỆP VIỆT NAM' },
    { value: 10, label: 'Du Lieu Du BAO:NÔNG NGHIỆP THẾ GIỚI' },

];

class ListPostOfWriter extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleUploadImage = this.handleUploadImage.bind(this);

        this.state = {
            Url: '',
            alert: [],
            Warning: [],
            user: [],
            imagePreviewURL: '',
            FullName: '',
            ShortDescription: '',
            FullDescription: '',
            UserID: '',
            avatar: '',
            CatID: null,
            imageSelect: null,
            submitted: false,
            error: '',
        }
    }
    handleSelect = CatID => {
        this.setState({ CatID });
        console.log(`Option selected:`, CatID);
    };

    componentWillMount() {
        this.setState({
            user: JSON.parse(localStorage.getItem('user')),
        });
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleImageChange = (e) => {
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            this.setState({
                imageSelect: file,
                imagePreviewURL: reader.result
            });
        }
        if (file != null) {
            reader.readAsDataURL(file)
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({ submitted: true });
        this.setState({ alert: [] });
        this.componentWillMount();
        if (this.state.imageSelect != null) {
            return this.handleUploadImage().then(res => {
                console.log(res)
                if (res != "" || res != null || res != undefined) {
                    var imgName = res;
                    this.fectSubmit(imgName);

                }
            });
        }
        else {
            var value = "warning";
            this.setState({ Warning: [] });
            return this.checkValueInput(value);
        }

    }

    fectSubmit = (imgName) => {
        fetch(`http://localhost:8000/posts/add`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                Url: this.state.Url,
                UserID: this.state.user.Id,
                FullName: this.state.FullName,
                ShortDescription: this.state.ShortDescription,
                FullDescription: this.state.FullDescription,
                CatID: this.state.CatID.value,
                avatar: imgName,
            })
        })
            .then(
                (result) => {
                    console.log(result);
                    if (result.statusText == "OK") {
                        var value = "success";
                        return this.checkValueInput(value);
                    }

                },
                (error) => {
                    console.log(error);
                }
            )
    }

    checkValueInput = (value) => {
        if (value == "warning") {
            var joined = this.state.Warning.concat(<div class="alert alert-warning">
                <strong>Warning!</strong> Please choose file IMG!
    </div>);
            this.setState({ Warning: joined })
            return this.state.warning;
        }
        else if (value == "success") {
            var joined = this.state.Warning.concat(<div class="alert alert-success">
                <strong>success!</strong> Add new post is success!
    </div>);
            this.setState({ Warning: joined });
            return this.state.Warning;
        }
    }

    handleUploadImage = () => {
        const data = new FormData()
        console.log(this.state.imageSelect);
        data.append('file', this.state.imageSelect)
        return PostService.pushImage(data).then(res => {
            console.log(res);
            return res.data;
        });

    }

    render() {
        console.log(this.state.user);
        const { Warning, CatID, avatar, Url, FullName, ShortDescription, FullDescription, error, submitted } = this.state;

        return (
            <writer>
                <h2>Post an article</h2>

                <form name="form" onSubmit={this.handleSubmit}>
                    <div class="PostCr">
                        {this.state.alert}
                        <label for="uname"><b>Url</b></label>
                        <input type="text" name="Url" value={Url} onChange={this.handleChange} />
                        {submitted && !Url &&
                            <div class="help-blocka">Current Url is required</div>
                        }

                        <label for="uname"><b>FullName</b></label>
                        <input type="text" name="FullName" value={FullName} onChange={this.handleChange} />
                        {submitted && !FullName &&
                            <div class="help-blocka">Current FullName is required</div>
                        }

                        <label for="uname"><b>ShortDescription</b></label>
                        <input type="text" name="ShortDescription" value={ShortDescription} onChange={this.handleChange} />
                        {submitted && !ShortDescription &&
                            <div class="help-blocka">Current ShortDescription is required</div>
                        }

                        <label for="uname"><b>FullDescription</b></label>
                        <input type="text" name="FullDescription" value={FullDescription} onChange={this.handleChange} />
                        {submitted && !FullDescription &&
                            <div class="help-blocka">Current FullDescription is required</div>
                        }

                        <label for="uname"><b>Category</b></label>
                        <Select
                            value={CatID}
                            onChange={this.handleSelect}
                            options={options}
                        />
                        {submitted && !CatID &&
                            <div class="help-blocka">Current Category is required</div>
                        }

                        <label for="uname"><b>Image</b></label>
                        <input type="file" name="avatar" value={avatar} onChange={this.handleImageChange} accept="image/*" className="form-control" />
                        <div className="w40p thumbnail pull-left" id="productImg">
                            <img src={this.state.imagePreviewURL} alt="vui lòng chọn hình" />
                        </div>
                        <button type="submit">Submit</button>
                    </div>
                    <br /><br />
                    {this.state.Warning}
                </form>
            </writer>
        );
    };
}
export default ListPostOfWriter;