import React, { Component } from 'react';
import PostService from '../../services/post.services'
import moment from 'moment';

class ListPostOfWriter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            post: [],
            samCat: [],
        }
    }

    componentWillMount() {
        this.setState({
            user: JSON.parse(localStorage.getItem('user')),
        });
        PostService.getDetailPost()
            .then(response => response.data)
            .then(data => {
                this.setState({
                    post: data
                });
                PostService.updateViews(data[0]).then(res => {
                    PostService.postsSameCategory(data[0])
                        .then(response => response.data)
                        .then(data => {
                            this.setState({
                                samCat: data
                            });
                        })
                        .catch(err => {
                            console.log(err);
                        });
                });
            })
            .catch(err => {
                console.log(err);
            });;
    }

    getPost = () => {
        let posts = this.state.post;
        var pos = [];
        if (posts.length > 0) {
            posts.forEach(function (item) {
                pos.push(
                    <div class="article-body">
                        <h1 class="article-titlePost">{item.FullName}</h1>
                        <div class="article-main-imgPost">
                            <img src={"http://localhost:8000/public/" + item.avatar} alt="" />
                        </div>
                        <ul class="article-info">
                            <li class="article-category"><a href="#">News</a></li>
                            <li class="article-type"><i class="fa fa-file-text"></i></li>
                        </ul>

                        <ul class="article-meta">
                            <li><i class="fa fa-clock-o"></i>{moment(item.createdAt).format("YYYY-MM-DD")}</li>
                            <li><i class="fa fa-comments"></i> 33</li>
                            <li><i class="fa fa-eye"></i> {item.views}</li>
                        </ul>
                        <p>{item.ShortDescription}.</p>
                        <h4>Hãy để lại chia sẻ của bạn ở cuối bài viết nhé, giờ thì cùng đến với phần nội dung chính bên dưới!</h4>
                        <p>{item.FullDescription}.</p>
                    </div>
                );
            });
        }
        return pos;
    }

    getPostsSameCategory = () => {
        let posts = this.state.samCat;
        var pos = [];
        if (posts.length > 0) {
            posts.forEach(function (item) {
                pos.push(<div className="col-md-3 col-sm-6">
                    <a href={"/post/" + item.Url} >
                        <article className="article">
                            <div className="article-listImg">
                                <a href={"/post/" + item.Url}>
                                    <img src={"http://localhost:8000/public/" + item.avatar} alt="" />
                                    <ul className="article-info">
                                        <li className="article-type"><i class="fa fa-camera"></i></li>
                                    </ul>
                                </a>
                            </div>
                            <div className="article-body">
                                <h4 className="article-title"><a href="#">{item.ShortName}</a></h4>
                                <ul className="article-meta">
                                    <li><i className="fa fa-clock-o"></i> {moment(item.createdAt).format("YYYY-MM-DD")}</li>
                                    <li><i className="fa fa-comments"></i> 33</li>
                                    <li><i class="fa fa-eye"></i> {item.views}</li>
                                </ul>
                            </div>
                        </article>
                    </a>
                </div>);
            });
        }
        return pos;
    }

    render() {
        console.log(this.state.user);
        return (
            <postwriter>
                <article class="article article-post">
                    <div class="article-share">
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
                    </div>
                    {this.getPost()}
                </article>
                <div className="home">
                    <div className="section-title">
                        <h2 className="title">Posts same category</h2>
                        <ul className="tab-nav pull-right">
                            <li className="active"><a data-toggle="tab" href="#tab1">All</a></li>
                            <li><a data-toggle="tab" href="#tab1">News</a></li>
                            <li><a data-toggle="tab" href="#tab1">Sport</a></li>
                            <li><a data-toggle="tab" href="#tab1">Music</a></li>
                            <li><a data-toggle="tab" href="#tab1">Business</a></li>
                            <li><a data-toggle="tab" href="#tab1">Lifestyle</a></li>
                        </ul>
                    </div>

                    <div className="tab-content">
                        <div id="tab1" class="tab-pane fade in active">
                            <div className="row">

                                {this.getPostsSameCategory()}

                            </div>
                        </div>
                    </div>
                </div>
            </postwriter >
        );
    };
}
export default ListPostOfWriter;