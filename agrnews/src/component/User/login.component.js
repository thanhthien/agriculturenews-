import React, { Component } from 'react';
import '../../assets/static_login/css/style.css';
import usersServices from '../../services/user.services';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';

const responseFacebook = (response) => {
    console.log(response);
}

const responseGoogle = (response) => {
    console.log(response);
}


class Login extends Component {
    constructor(props) {
        super(props);
        usersServices.logout();
        var self = this;
        this.handleSubmit = self.handleSubmit.bind(this);
        this.state = {
            alert: [],

        };
    }

    componentWillMount() {
    }
    componentDidMount() {
        const $ = window.$;
        $('#loginForm').validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true

                },
                dob: {
                    required: true

                },
                password: {
                    required: true,
                    minlength: 6
                }
            },
            messages: {
                name: {
                    required: 'account or password is incorrect.',
                    valid: 'account or password is incorrect.'
                },
                password: {
                    required: 'account or password is incorrect',
                    minlength: 'account or password is incorrect',
                }
            },
            errorElement: 'small',
            errorClass: "text-danger",
            validClass: 'is-valid',
            highlight: function (e) {
                $(e).removeClass('is-valid').addClass('is-valid')
            },
            unhighlight: function (e) {
                $(e).removeClass('is-valid').addClass('is-valid')
            }
        })
    }


    handleSubmit(event) {
        event.preventDefault();
        const $ = window.$,
            form = $("#loginForm"),
            format = /^[a-zA-Z0-9]*$/,
            user_Name = event.target.name.value,
            user_Password = event.target.password.value;
        this.setState({ alert: [] });
        if (form.valid() == true) {
            if (format.test(user_Name) == true) {
                usersServices.checkLoginUser(user_Name, user_Password)
                    .then(res => {
                        if (!res) {
                            var joined = this.state.alert.concat(<div class="alert alert-danger">
                                <strong>Danger!</strong> Invalid password!
                        </div>);
                            this.setState({ alert: joined })
                            return this.state.alert;
                        }
                        else {
                            const { from } = this.props.location.state || { from: { pathname: "/" } };
                            return this.props.history.push(from);
                        }

                    })
            }
            else {
                alert("Username contains characters invalid!");
            }
        }
    }

    render() {
        return (

            <div class="main">
                <div class="main">
                    <section class="signup">
                        <div class="container">
                            <div class="signup-content">
                                <form method="POST" id="loginForm" onSubmit={this.handleSubmit} >
                                    <h2 class="form-title">Sign in</h2>
                                    {this.state.alert}
                                    <div class="form-group">
                                        <input type="text" class="form-input" name="name" id="name" placeholder="Your Name" />
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-input" name="password"
                                            id="password" placeholder="Password" />
                                        <span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" name="submit" id="submit" class="form-submit" value="Sign in" />
                                    </div>

                                    <FacebookLogin
                                        appId="483120605775833" //APP ID NOT CREATED YET
                                        fields="name,email,picture"
                                        callback={responseFacebook}
                                    />
                                    <br />
                                    <br />
                                    <GoogleLogin
                                        clientId="265025909115-7tjhrdtnunfgdu2c3mlj859jnii1qq1h.apps.googleusercontent.com" //CLIENTID NOT CREATED YET
                                        buttonText="LOGIN WITH GOOGLE"
                                        onSuccess={responseGoogle}
                                        onFailure={responseGoogle}
                                    />
                                    <br />
                                    <br />
                                </form>
                                <p class="loginhere">
                                    No account ? <a href="http://localhost:3000/register" class="loginhere-link">Register here</a>
                                </p>
                                <p class="forwardaccount">
                                    Forward account ? <a href="http://localhost:3000/reset" class="loginhere-link">Reset Your Password</a>
                                </p>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

        );
    };
}
export default Login;