import React, { Component } from 'react';
import '../../../assets/static_login/account.style.css';
import userService from '../../../services/user.services'
var moment = require('moment');

class Account extends Component {
    constructor(props) {
        super(props);
        var self = this;
        this.state = {
            alert: [],
            password: '',
            newPassword: '',
            submitted: false,
            error: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    componentWillMount() {
        this.setState({
            user: JSON.parse(localStorage.getItem('user')),
        });
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ submitted: true });
        this.setState({ alert: [] });
        const { username, password, newPassword } = this.state;
        if (!(password && newPassword)) {

            return;
        }
        userService.changePassword(this.state.user.Id, this.state.user.Username, password, newPassword)
            .then(res => {
                if (res == "success") {
                    var joined = this.state.alert.concat(<div class="alert alert-success">
                        <strong>Change password is</strong> {res}
                    </div>);
                    this.setState({ alert: joined })
                    return this.state.alert;
                }
                else {
                    var joined = this.state.alert.concat(<div class="alert alert-danger">
                        <strong>Danger!</strong> {res}
                    </div>);
                    this.setState({ alert: joined })
                    return this.state.alert;
                }
            }).catch(err=>{
                console.log(err)
            })
            
    }

    render() {
        console.log(this.state.user);
        if(!this.state.user){
            const { from } = this.props.location.state || { from: { pathname: "/login" } };
            return this.props.history.push(from);
        }
        const { password, username, newPassword, error, submitted } = this.state;
        return (
            <div class="account">
                <body>
                    <h2>Change Password</h2>
                    <form name="form" onSubmit={this.handleSubmit}>
                        <div class="container">
                        {this.state.alert}
                            <label for="uname"><b>Username</b></label>
                            <input type="text" name="username" value={username} defaultValue={this.state.user.Username} readOnly />

                            <label for="psw"><b>Current Password</b></label>
                            <input type="password" name="password" value={password} onChange={this.handleChange} minlength="6" />
                            {submitted && !password &&
                                <div class="help-blocka">Current Password is required</div>
                            }
                            <label for="psw"><b>New PassWord</b></label>
                            <input type="password" name="newPassword" value={newPassword} onChange={this.handleChange} minlength="6" />
                            {submitted && !newPassword &&
                                <div class="help-blocka">New PassWord is required</div>
                            }
                            <button type="submit">Change PassWord</button>
                        </div><br />
                    </form>
                </body>
            </div>

        );
    };
}
export default Account;