import React, { Component } from 'react';
import '../../../assets/static_login/account.style.css';
import avatar from '../../../assets/static_login/images/_Ninja-2-512.png';
var moment = require('moment');

class Account extends Component {
    constructor(props) {
        super(props);
        var self = this;
        this.state = {
            user: []
        }
    }
    componentWillMount() {
        this.setState({
            user: JSON.parse(localStorage.getItem('user')),
        });

    }


    render() { 
        console.log(this.state.user);
        if(this.state.user== null){
            console.log(this.state.user)
            const { from } = this.props.location.state || { from: { pathname: "/account" } };
            return this.props.history.push(from);
        }
        return (
            <div class="account">
                <body>
                    <h2>Account User</h2>
                    <form action="">
                        <div class="imgcontainer">
                            <img src={avatar} alt="Avatar" class="avatar" />
                        </div>

                        <div class="container">
                            <label for="uname"><b>Username</b></label>
                            <input type="text" name="uname" defaultValue={this.state.user.Username} readOnly />

                            <label for="psw"><b>email</b></label>
                            <input type="text" name="psw" defaultValue={this.state.user.Email} readOnly />

                            <label for="psw"><b>Day Of Birth</b></label>
                            <input type="text" name="psw" defaultValue={moment(this.state.user.Dob).format("YYY-MM-DD")} readOnly />

                            <label for="psw"><b>Status</b></label>
                            <input type="text" name="psw" defaultValue={this.state.user.UserRights} readOnly />

                            <button type="submit">Login</button>
                        </div><br />
                    </form>
                </body>
            </div>

        );
    };
}
export default Account;