import React, { Component } from 'react';
import '../../assets/static_login/css/style.css'
import usersServices from '../../services/user.services'

class Reset extends Component {
    constructor(props) {
        super(props);
        usersServices.logout();
        var self = this;
        this.handleSubmit = self.handleSubmit.bind(this);
        this.state = {
            alert: [],

        };
    }

    componentWillMount() {
    }
    componentDidMount() {
        const $ = window.$;
        $('#loginForm').validate({
            rules: {
                email: {
                    required: true

                },
            },
            messages: {
                email: {
                    required: 'Email your is incorrect.',
                },
            },
            errorElement: 'small',
            errorClass: "text-danger",
            validClass: 'is-valid',
            highlight: function (e) {
                $(e).removeClass('is-valid').addClass('is-valid')
            },
            unhighlight: function (e) {
                $(e).removeClass('is-valid').addClass('is-valid')
            }
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        const $ = window.$,
            email = event.target.email.value,
            form = $("#loginForm");
        this.setState({ alert: [] });
        if (form.valid() == true) {
            usersServices.resetEmail(email)
                .then(res => {
                    if (res == "fail") {
                        var joined = this.state.alert.concat(<div class="alert alert-danger">
                            <strong>Danger!</strong> Email does not exist!
                        </div>);
                        this.setState({ alert: joined })
                        return this.state.alert;
                    }
                    else if(res == "success"){
                        var joined = this.state.alert.concat(<div class="alert alert-success">
                            <strong>Success!</strong> Please check your Email!
                        </div>);
                        this.setState({ alert: joined })
                        return this.state.alert;
                    }

                })

        }
    }

    render() {
        return (
            <div class="main">
                <div class="main">
                    <section class="signup">
                        <div class="container">
                            <div class="signup-content">
                                <form method="POST" id="loginForm" onSubmit={this.handleSubmit} >
                                    <h2 class="form-title">Reset Your Password</h2>
                                    {this.state.alert}
                                    <div class="form-group">
                                        <input type="email" class="form-input" name="email" id="email" placeholder="Your Email Reset" />
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" name="submit" id="submit" class="form-submit" value="Reset" />
                                    </div>
                                </form>
                                <p class="loginhere">
                                    <a href="http://localhost:3000/login" class="loginhere-link">Login Here</a>
                                </p>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

        );
    };
}
export default Reset;