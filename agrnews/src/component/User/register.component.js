import React, { Component } from 'react';
import '../../assets/static_login/css/style.css'
import ReCAPTCHA from "react-google-recaptcha";
import User_Register from '../../services/user.services'
import Notifications, { notify } from 'react-notify-toast';
import * as constants from "../../common/constants"
import usersServices from '../../services/user.services'
const TEST_SITE_KEY = "6Lcxbl8UAAAAAAey58seZQq3DAZVboYJp8RkA0L7";




class Register extends Component {
    constructor(props) {
        super(props);
        var self = this;
        this.handleSubmit = self.handleSubmit.bind(this);
        this.state = {
            name: '',
            email: '',
            dob: '',
            password: '',
            captcha: null,
        };

    }

    componentWillMount() {
    }


    componentDidMount() {
        const $ = window.$;
        $('#registerForm').validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true

                },
                dob: {
                    required: true

                },
                password: {
                    required: true,
                    minlength: 6
                }
            },
            messages: {
                name: {
                    required: 'Your username is required.',
                    valid: 'Username contains characters invalid!'
                },
                email: {
                    required: 'Your email is required.'
                },
                dob: {
                    required: 'Your Date Of Birth is required.'
                },
                password: {
                    required: 'Your password is required.',
                    minlength: 'The length of pass must be greater than 6.',
                }
            },
            errorElement: 'small',
            errorClass: "text-danger",
            validClass: 'is-valid',
            highlight: function (e) {
                $(e).removeClass('is-valid').addClass('is-valid')
            },
            unhighlight: function (e) {
                $(e).removeClass('is-valid').addClass('is-valid')
            }
        })
    }

    verifyCallback = (data) => {
        console.log(data);
        this.setState({ captcha: data });
    }



    handleSubmit(event) {
        event.preventDefault();
        const data_form = event.target;
        const $ = window.$,
            form = $("#registerForm"),
            format = /^[a-zA-Z0-9]*$/,
            user_Name = event.target.name.value,
            user_Email = event.target.email.value;
        if (form.valid() == true) {
            if (format.test(user_Name) == true) {
                console.log(this.state.captcha);
                if (this.state.captcha != null) {
                    usersServices.checkNameValue(user_Name, user_Email)
                        .then(response => {
                            console.log(response)
                            if (response == true) {
                                User_Register.getFormRegister(data_form)
                                    .then(res => {
                                        if (res == 200) {
                                            let myColor = { background: '#0E1717', text: "#f1f3f6" };
                                            notify.show("Account successfully created!", "custom", 5000, myColor);
                                        }
                                    })
                                    .catch(err => {
                                        console.log(err);
                                    });
                            }
                            if (response == false) {
                                alert("Please choose another name or another email!");
                            }
                        })
                } else {
                    alert("Catcha contains characters invalid!");

                }
            } else {
                alert("Username contains characters invalid!");
            }
        }
    }

    render() {
        return (
            <div class="main">
                <div class="main">
                    <Notifications options={{ text: "#21c912" }} />
                    <section class="signup">
                        <div class="container">
                            <div class="signup-content">
                                <form method="POST" id="registerForm" onSubmit={this.handleSubmit}>
                                    <h2 class="form-title">Create account</h2>
                                    <div class="form-group">
                                        <input type="text" class="form-input" name="name" id="name" placeholder="Your Name" />
                                    </div>

                                    <div class="form-group">
                                        <input type="email" class="form-input" name="email" id="email" placeholder="Your Email" />
                                    </div>
                                    <div class="form-group">
                                        <input type='date' class="form-control" name="dob" id="date" type="date" placeholder="Date Of Birth" max="9999-00-00"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-input" name="password"
                                            id="password" placeholder="Password" />
                                        <span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span>
                                    </div>
                                    <div class="form-group">
                                        <ReCAPTCHA
                                            style={{ display: "inline-block" }}
                                            theme="dark"
                                            ref="recaptcha"
                                            sitekey={TEST_SITE_KEY}
                                            onChange={this.verifyCallback}
                                        />
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" name="submit" id="submit" class="form-submit" value="register" />
                                    </div>
                                </form>
                                <p class="loginhere">
                                    Have already an account ? <a href="http://localhost:3000/login" class="loginhere-link">Login here</a>
                                </p>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

        );
    };
}
export default Register;