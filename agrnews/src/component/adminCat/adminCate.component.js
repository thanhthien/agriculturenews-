import React, { Component } from 'react';
import CategoryServices from '../../services/category.services';
import Modal from 'react-modal';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const style = {
    color: "#008B8B",

}
const input = {
    width: "100%",

}

const customStyles = {
    content: {
        top: '60%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};
Modal.setAppElement(document.createElement('div'))
class AdminCat extends React.Component {
    constructor(props) {
        super(props);
        this.loadAllCategory = this.loadAllCategory.bind(this);
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            list: [],
            user: [],
            modalIsOpen: false,
            UserRights: '',
            UserAdmin: '',
            CatID: '',
            CatName: '',
            Url: '',
            ParentIDS: '',
            Id: '',

        }

    }
    componentWillMount() {
        this.setState({
            user: JSON.parse(localStorage.getItem('user')),
        });
    }

    componentDidMount() {
        return this.loadAllCategory();
    }

    deleteCat = (item) => {
        var r = window.confirm("Are you sure you want to delete this Categgory");
        if (r == true) {
            var CatID = item.CatID;
            CategoryServices.deleteCat(CatID).then(res => {
                if (res.data == "success") {
                    this.loadAllCategory();
                    NotificationManager.success('Delete Category is success!', 'Message');
                }
                else
                    NotificationManager.warning('Warning message', 'Close after 3000ms', 3000);

            });

        }
    }

    loadAllCategory = () => {
        var self = this;
        CategoryServices.loadListCatByAdmin().then(res => {
            let categories = res.data;
            var cate = [];
            if (categories.length > 0) {
                categories.forEach(function (item, ) {
                    if (item.children.length > 0) {
                        const child = item.children.map((itemChild, i) =>
                            <tr>
                                <td key={i}>{itemChild.CatID}</td>
                                <td key={i}>{itemChild.CatName}</td>
                                <td key={i}>{itemChild.Url}</td>
                                <td key={i}>{itemChild.ParentIDS}</td>
                                <td><button type="button" class="btn btn-info" onClick={() => self.openModal(itemChild)} ><i class="fa fa-pencil-square-o"></i> Detail</button></td>
                                <td><button type="button" class="btn btn-danger" onClick={() => self.deleteCat(itemChild)} ><i class="fa fa-remove"></i> Delete</button></td>
                            </tr>);
                        cate.push(<tr>
                            <td style={style}>{item.CatID}</td>
                            <td style={style}>{item.CatName}</td>
                            <td style={style}>{item.Url}</td>
                            <td style={style}>{item.ParentIDS}</td>
                            <td style={style}><button type="button" class="btn btn-info" onClick={() => self.openModal(item)} ><i class="fa fa-pencil-square-o"></i> Detail</button></td>
                            <td style={style}><button type="button" class="btn btn-danger" onClick={() => self.deleteCat(item)} ><i class="fa fa-remove"></i> Delete</button></td>
                        </tr>);
                        cate.push(child);
                    }
                    else {
                        cate.push(<tr>
                            <td style={style}>{item.CatID}</td>
                            <td style={style}>{item.CatName}</td>
                            <td style={style}>{item.Url}</td>
                            <td style={style}>{item.ParentIDS}</td>
                            <td style={style}><button type="button" class="btn btn-info" onClick={() => self.openModal(item)} ><i class="fa fa-pencil-square-o"></i> Detail</button></td>
                            <td style={style}><button type="button" class="btn btn-danger" onClick={() => self.deleteCat(item)} ><i class="fa fa-remove"></i> Delete</button></td>
                        </tr>);
                    }
                });
                self.setState({
                    list: cate,
                    modalIsOpen: false,
                });
                return this.state.list;
            }
        })
    };
    handleChange(event) {
        event.preventDefault();
        this.setState({
            CatID: event.target.CatID,
            CatName: event.target.CatName,
            Url: event.target.Url,
            ParentIDS: event.target.ParentIDS,

        });
    };

    handleSubmit(event) {
        event.preventDefault();
        var CatID = event.target.CatID.value,
            CatName = event.target.CatName.value,
            Url = event.target.Url.value,
            ParentIDS = event.target.ParentIDS.value;
        CategoryServices.updateCat(CatID, CatName, Url, ParentIDS).then(res => {
            if (res.data == "success") {
                this.loadAllCategory();
                this.setState({ modalIsOpen: false });
                NotificationManager.success('Update user is success!', 'Message');
            }
            else
                NotificationManager.warning('Warning message', 'Close after 3000ms', 3000);

        });

    };

    openModal(item) {
        this.setState({
            CatID: item.CatID,
            CatName: item.CatName,
            Url: item.Url,
            ParentIDS: item.ParentIDS,
            modalIsOpen: true
        });
    };

    afterOpenModal = () => {
        this.subtitle.style.color = '#5F9EA0';
    };

    closeModal = () => {
        this.setState({ modalIsOpen: false });
    };

    render() {
        return (
            <postwriter>
                <NotificationContainer />
                <h2>Categories Management</h2>
                <div class="form-group">
                    <a href="http://127.0.0.1:3000/addCat"><button type="submit" name="submit" id="submit" class="form-submit">Add Category</button></a>
                </div>
                <table className="listPosts">
                    <tr>
                        <th width="30px">CatID</th>
                        <th>CatName</th>
                        <th>Url</th>
                        <th>ParentIDS</th>
                        <th width="20px">Detail</th>
                        <th width="20px">Delete</th>
                    </tr>
                    {this.state.list}
                </table>
                <div className="modalPostAdmin">
                    <Modal
                        isOpen={this.state.modalIsOpen}
                        onAfterOpen={this.afterOpenModal}
                        onRequestClose={this.closeModal}
                        style={customStyles}
                        contentLabel="Post Modal"
                    >
                        <h2 ref={subtitle => this.subtitle = subtitle}>Detailed Article (Admin)</h2>
                        <div className="modalArticle">
                            <form onSubmit={this.handleSubmit}>
                                <div class="form-group">
                                    <label>CatID: <input class="form-control" type="text" name="CatID" id="CatID" onChange={this.handleChange} value={this.state.CatID} />
                                    </label></div>
                                <div class="form-group">
                                    <label>CatName: <input class="form-control" type="text" name="CatName" id="CatName" onChange={this.handleChange} value={this.state.CatName} />
                                    </label>  </div>
                                <div class="form-group">
                                    <label> Url: <input class="form-control" type="text" name="Url" id="Url" onChange={this.handleChange} value={this.state.Url} />
                                    </label>  </div>
                                <div class="form-group">
                                    <label>ParentIDS: <input class="form-control" type="text" name="ParentIDS" id="ParentIDS" onChange={this.handleChange} value={this.state.ParentIDS} />
                                    </label>  </div>
                                <div class="form-group">
                                    <button type="submit" name="submit" id="submit" class="form-submit">update</button>
                                </div>
                            </form>
                            <button type="submit" name="submit" id="submit" class="form-submit" onClick={this.closeModal}>close</button>
                        </div>
                    </Modal>
                </div>
            </postwriter>

        );
    };
}
export default AdminCat;