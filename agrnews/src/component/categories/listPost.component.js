import React, { Component } from 'react';
import moment from 'moment';
import postServices from '../../services/post.services';

import Pagination from "react-js-pagination";

class listPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pageIndex: 1,
            posts: [],
        }
    }

    getPage = (pageIndex) => {
        var self = this;
        var pageSize = 6
        if (pageIndex == undefined) {
             pageIndex = 0
        }
        postServices.getListPostWithPage(pageIndex, pageSize)
            .then(response => response.data)
            .then(data => {
                console.log("1111111111")
                console.log(data)
                self.setState({
                    posts: data
                });
                // this.setState({
                //     posts: data
                // }, () => {
                //     this.getListOfArticle();
                // });
            })
            .catch(err => {
            });
    }
    componentWillMount() {
        this.getPage(0);

    }
    componentDidMount() {
        this.getPage();
    }

    getListName = () => {
        if (this.state.posts.length > 0) {
            var listPos = [];
            var nameParent = this.state.posts[0].NameParent;
            var CatName = this.state.posts[0].CatName;
            listPos.push(
                <h2 className="title">{nameParent + ": " + CatName}</h2>
            )
        }
        return listPos
    }
    getListOfArticle = () => {
        let posts = this.state.posts;
        var pos = [];
        if (posts.length > 0) {
            posts.forEach(function (item) {
                pos.push(
                    <article class="article row-article">
                        <div class="article-img">
                            <a href={"/post/" + item.Url} >
                                <img src={"http://localhost:8000/public/" + item.avatar} alt="" />
                            </a>
                        </div>
                        <div class="article-body">
                            <ul class="article-info">
                                <li class="article-category"><a href="#">News</a></li>
                                <li class="article-type"><i class="fa fa-file-text"></i></li>
                            </ul>
                            <h3 class="article-title"><a href="#">{item.FullName}</a></h3>
                            <ul class="article-meta">
                                <li><i class="fa fa-clock-o"></i>{moment(item.createdAt).format("YYYY-MM-DD")}</li>
                                <li><i class="fa fa-comments"></i> 33</li>
                            </ul>
                            <p>{item.ShortDescription}</p>
                        </div>
                    </article>)
            });
        }
        return pos;
    }
    render() {
        console.log(this.props);
        return (
            <home>
                <div className="listPost"><br />
                    <div className="section-title"><br />
                        {this.getListName()}
                        <ul className="tab-nav pull-right">
                            <li className="active"><a data-toggle="tab" href="#tab1">All</a></li>
                            <li><a data-toggle="tab" href="#tab1">News</a></li>
                            <li><a data-toggle="tab" href="#tab1">Sport</a></li>
                            <li><a data-toggle="tab" href="#tab1">Music</a></li>
                            <li><a data-toggle="tab" href="#tab1">Business</a></li>
                            <li><a data-toggle="tab" href="#tab1">Lifestyle</a></li>
                        </ul>
                    </div>
                    <div className="posts">

                        {this.getListOfArticle()}

                    </div>
                    <Pagination
                        pageIndex={this.state.pageIndex}
                        itemsCountPerPage={10}
                        totalItemsCount={30}
                        onChange={this.componentWillMount}
                    />
                </div>
            </home>
        );
    };
}
export default listPost;