import React from 'react';
import logo_alt from '../assets/img/logo-alt.png'
import img1 from '../assets/img/img-widget-1.jpg'
import img2 from '../assets/img/img-widget-2.jpg'
import img3 from '../assets/img/img-widget-3.jpg'
import img4 from '../assets/img/img-widget-4.jpg'
import img5 from '../assets/img/img-widget-5.jpg'
import img6 from '../assets/img/img-widget-6.jpg'
import img7 from '../assets/img/img-widget-7.jpg'
import img8 from '../assets/img/img-widget-8.jpg'
import img9 from '../assets/img/img-widget-9.jpg'
import img10 from '../assets/img/img-widget-10.jpg'

function Footer() {
    return (
		<footer id="footer">
			<div id="top-footer" className="section">
				<div className="container">
					<div className="row">
						<div className="col-md-4">
							<div className="footer-widget about-widget">
								<div className="footer-logo">
									<a href="#" className="logo"><img src={logo_alt} alt="logo_alt"/></a>
									<p>Populo tritani laboramus ex mei, no eum iuvaret ceteros euripidis, ne alia sadipscing mei. Te inciderint cotidieque pro, ei iisque docendi qui.</p>
								</div>
							</div>
							
							<div className="footer-widget social-widget">
								<div className="widget-title">
									<h3 className="title">Follow Us</h3>
								</div>
								<ul>
									<li><a href="#" className="facebook"><i className="fa fa-facebook"></i></a></li>
									<li><a href="#" className="twitter"><i className="fa fa-twitter"></i></a></li>
									<li><a href="#" className="google"><i className="fa fa-google"></i></a></li>
									<li><a href="#" className="instagram"><i className="fa fa-instagram"></i></a></li>
									<li><a href="#" className="youtube"><i className="fa fa-youtube"></i></a></li>
									<li><a href="#" className="rss"><i className="fa fa-rss"></i></a></li>
								</ul>
							</div>
							
							<div className="footer-widget subscribe-widget">
								<div className="widget-title">
									<h2 className="title">Subscribe to Newslatter</h2>
								</div>
								<form>
									<input className="input" type="email" placeholder="Enter Your Email"/>
									<button className="input-btn">Subscribe</button>
								</form>
							</div>
						</div>
						
						<div className="col-md-4">
							<div className="footer-widget">
								<div className="widget-title">
									<h2 className="title">Featured Posts</h2>
								</div>

								<article className="article widget-article">
									<div className="article-img">
										<a href="#">
											<img src={img1} alt="img1"/>
										</a>
									</div>
									<div className="article-body">
										<h4 className="article-title"><a href="#">Duis urbanitas eam in, tempor consequat.</a></h4>
										<ul className="article-meta">
											<li><i className="fa fa-clock-o"></i> January 31, 2017</li>
											<li><i className="fa fa-comments"></i> 33</li>
										</ul>
									</div>
								</article>
								
								<article className="article widget-article">
									<div className="article-img">
										<a href="#">
											<img src={img2} alt="img2"/>
										</a>
									</div>
									<div className="article-body">
										<h4 className="article-title"><a href="#">Duis urbanitas eam in, tempor consequat.</a></h4>
										<ul className="article-meta">
											<li><i className="fa fa-clock-o"></i> January 31, 2017</li>
											<li><i className="fa fa-comments"></i> 33</li>
										</ul>
									</div>
								</article>
								
								<article className="article widget-article">
									<div className="article-img">
										<a href="#">
											<img src={img3} alt="img3"/>
										</a>
									</div>
									<div className="article-body">
										<h4 className="article-title"><a href="#">Duis urbanitas eam in, tempor consequat.</a></h4>
										<ul className="article-meta">
											<li><i className="fa fa-clock-o"></i> January 31, 2017</li>
											<li><i className="fa fa-comments"></i> 33</li>
										</ul>
									</div>
								</article>
							</div>
						</div>
						
						<div className="col-md-4">
							<div className="footer-widget galery-widget">
								<div className="widget-title">
									<h2 className="title">Flickr Photos</h2>
								</div>
								<ul>
									<li><a href="#"><img src={img3} alt=""/></a></li>
									<li><a href="#"><img src={img4} alt=""/></a></li>
									<li><a href="#"><img src={img5} alt=""/></a></li>
									<li><a href="#"><img src={img6} alt=""/></a></li>
									<li><a href="#"><img src={img7} alt=""/></a></li>
									<li><a href="#"><img src={img8} alt=""/></a></li>
									<li><a href="#"><img src={img9} alt=""/></a></li>
									<li><a href="#"><img src={img10} alt=""/></a></li>
								</ul>
							</div>
							
							<div className="footer-widget tweets-widget">
								<div className="widget-title">
									<h2 className="title">Recent Tweets</h2>
								</div>
								<ul>
									<li className="tweet">
										<i className="fa fa-twitter"></i>
										<div className="tweet-body">
											<p><a href="#">@magnews</a> Populo tritani laboramus ex mei, no eum iuvaret ceteros euripidis <a href="#">https://t.co/DwsTbsmxTP</a></p>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="bottom-footer" className="section">
				<div className="container">
					<div className="row">
						<div className="col-md-6 col-md-push-6">
							<ul className="footer-links">
								<li><a href="#">About us</a></li>
								<li><a href="#">Contact</a></li>
								<li><a href="#">Advertisement</a></li>
								<li><a href="#">Privacy</a></li>
							</ul>
						</div>
						<div className="col-md-6 col-md-pull-6">
							<div className="footer-copyright">
								<span>
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i className="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>

    );
}
export default Footer