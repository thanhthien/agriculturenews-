import React, { Component } from 'react';
import moment from 'moment';
import postServices from '../../services/post.services';

class ListPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            posts10new: [],
            posts10Views: [],
        }
    }

    componentWillMount() {
        postServices.getAll()
            .then(response => response.data)
            .then(data => {
                this.setState({
                    posts: data
                });
            })
            .catch(err => {
                console.log(err);
            });

        postServices.getAllNews()
            .then(response => response.data)
            .then(data => {
                this.setState({
                    posts10new: data
                });
            })
            .catch(err => {
                console.log(err);
            });

        postServices.getAllViews()
            .then(response => response.data)
            .then(data => {
                this.setState({
                    posts10Views: data
                });
            })
            .catch(err => {
                console.log(err);
            });

    }

    getPost = () => {
        let posts = this.state.posts;
        var pos = [];
        if (posts.length > 0) {
            posts.forEach(function (item) {
                pos.push(<div className="col-md-3 col-sm-6">
                    <a href={"/post/" + item.Url} >
                        <article className="article">
                            <div className="article-listImg">
                                <a href={"/post/" + item.Url}>
                                    <img src={"http://localhost:8000/public/" + item.avatar} alt="" />
                                    <ul className="article-info">
                                        <li className="article-type"><i class="fa fa-camera"></i></li>
                                    </ul>
                                </a>
                            </div>
                            <div className="article-body">
                                <h4 className="article-title"><a href="#">{item.ShortName}</a></h4>
                                <ul className="article-meta">
                                    <li><i className="fa fa-clock-o"></i> {moment(item.createdAt).format("YYYY-MM-DD")}</li>
                                    <li><i className="fa fa-comments"></i> 33</li>
                                    <li><i class="fa fa-eye"></i> {item.views}</li>
                                </ul>
                            </div>
                        </article>
                    </a>
                </div>);
            });
        }
        return pos;
    }

    getPostNews = () => {
        let posts = this.state.posts10new;
        var pos = [];
        if (posts.length > 0) {
            posts.forEach(function (item) {
                pos.push(<div className="col-md-3 col-sm-6">
                    <a href={"/post/" + item.Url} >
                        <article className="article">
                            <div className="article-listImg">
                                <a href={"/post/" + item.Url}>
                                    <img src={"http://localhost:8000/public/" + item.avatar} alt="" />
                                    <ul className="article-info">
                                        <li className="article-type"><i class="fa fa-camera"></i></li>
                                    </ul>
                                </a>
                            </div>
                            <div className="article-body">
                                <h4 className="article-title"><a href="#">{item.ShortName}</a></h4>
                                <ul className="article-meta">
                                    <li><i className="fa fa-clock-o"></i> {moment(item.createdAt).format("YYYY-MM-DD")}</li>
                                    <li><i className="fa fa-comments"></i> 33</li>
                                    <li><i class="fa fa-eye"></i> {item.views}</li>
                                </ul>
                            </div>
                        </article>
                    </a>
                </div>);
            });
        }
        return pos;
    }

    getPostMultiple = () => {
        let posts = this.state.posts10Views;
        var pos = [];
        if (posts.length > 0) {
            posts.forEach(function (item) {
                pos.push(<div className="col-md-3 col-sm-6">
                    <a href={"/post/" + item.Url} >
                        <article className="article">
                            <div className="article-listImg">
                                <a href={"/post/" + item.Url}>
                                    <img src={"http://localhost:8000/public/" + item.avatar} alt="" />
                                    <ul className="article-info">
                                        <li className="article-type"><i class="fa fa-camera"></i></li>
                                    </ul>
                                </a>
                            </div>
                            <div className="article-body">
                                <h4 className="article-title"><a href="#">{item.ShortName}</a></h4>
                                <ul className="article-meta">
                                    <li><i className="fa fa-clock-o"></i> {moment(item.createdAt).format("YYYY-MM-DD")}</li>
                                    <li><i className="fa fa-comments"></i> 33</li>
                                    <li><i class="fa fa-eye"></i> {item.views}</li>
                                </ul>
                            </div>
                        </article>
                    </a>
                </div>);
            });
        }
        return pos;
    }

    render() {
        return (
            <home>
                <div className="home">
                    <div className="section-title">
                        <h2 className="title">Hot Trending Posts </h2>
                        <ul className="tab-nav pull-right">
                            <li className="active"><a data-toggle="tab" href="#tab1">All</a></li>
                            <li><a data-toggle="tab" href="#tab1">News</a></li>
                            <li><a data-toggle="tab" href="#tab1">Sport</a></li>
                            <li><a data-toggle="tab" href="#tab1">Music</a></li>
                            <li><a data-toggle="tab" href="#tab1">Business</a></li>
                            <li><a data-toggle="tab" href="#tab1">Lifestyle</a></li>
                        </ul>
                    </div>
                    <div className="tab-content">
                        <div id="tab1" class="tab-pane fade in active">
                            <div className="row">
                                {this.getPost()}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="home">
                    <div className="section-title">
                        <h2 className="title">Top 10 latest posts</h2>
                        <ul className="tab-nav pull-right">
                            <li className="active"><a data-toggle="tab" href="#tab1">All</a></li>
                            <li><a data-toggle="tab" href="#tab1">News</a></li>
                            <li><a data-toggle="tab" href="#tab1">Sport</a></li>
                            <li><a data-toggle="tab" href="#tab1">Music</a></li>
                            <li><a data-toggle="tab" href="#tab1">Business</a></li>
                            <li><a data-toggle="tab" href="#tab1">Lifestyle</a></li>
                        </ul>
                    </div>
                    <div className="tab-content">
                        <div id="tab1" class="tab-pane fade in active">
                            <div className="row">
                                {this.getPostNews()}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="home">
                    <div className="section-title">
                        <h2 className="title">Top 10 most viewed articles</h2>
                        <ul className="tab-nav pull-right">
                            <li className="active"><a data-toggle="tab" href="#tab1">All</a></li>
                            <li><a data-toggle="tab" href="#tab1">News</a></li>
                            <li><a data-toggle="tab" href="#tab1">Sport</a></li>
                            <li><a data-toggle="tab" href="#tab1">Music</a></li>
                            <li><a data-toggle="tab" href="#tab1">Business</a></li>
                            <li><a data-toggle="tab" href="#tab1">Lifestyle</a></li>
                        </ul>
                    </div>
                    <div className="tab-content">
                        <div id="tab1" class="tab-pane fade in active">
                            <div className="row">
                                {this.getPostMultiple()}
                            </div>
                        </div>
                    </div>
                </div>
            </home>
        );
    };
}
export default ListPost;