import React, { Component } from 'react';
import moment from 'moment';
import UserServices from '../../services/user.services';

import Modal from 'react-modal';
import Select from 'react-select';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
const options = [
    { value: 'Subscriber', label: 'Subscriber' },
    { value: 'Writer', label: 'Writer' },
    { value: 'Editor', label: 'Editor' },
    { value: 'Admin', label: 'Admin' },
];

const customStyles = {
    content: {
        top: '60%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};
Modal.setAppElement(document.createElement('div'))
class AdminUser extends React.Component {
    constructor(props) {
        super(props);
        this.loadAllUser = this.loadAllUser.bind(this);
        this.state = {
            list: [],
            user: [],
            modalIsOpen: false,
            UserRights: '',
            UserAdmin: '',
            Id: '',

        }
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }


    componentWillMount() {
        this.setState({
            user: JSON.parse(localStorage.getItem('user')),
        });
    }
    componentDidMount() {
        return this.loadAllUser();
    }

    handleSelect = UserRights => {
        this.setState({ UserRights: UserRights.value });
    };

    deleteUser = (item) => {
        var userAdmin = item.UserRights;
        if (userAdmin == "Admin") {
            alert("Administrator is not deleted");
        } 
        else {
            var r = window.confirm("Are you sure you want to delete this user");
            if (r == true) {
                var Id = item.Id;
                UserServices.deleteUserServices(Id).then(res => {
                    if (res.data == "success") {
                        this.loadAllUser();
                        NotificationManager.success('Delete user is success!', 'Message');
                    }
                    else
                        NotificationManager.warning('Warning message', 'Close after 3000ms', 3000);
    
                });
            } 
        }
    }

    updateUserRights = () => {
        var userAdmin = this.state.UserAdmin;
        if (userAdmin == "Admin") {
            alert("Administrator is not edited");
        }
        else {
            var UserRights = this.state.UserRights;
            var Id = this.state.Id;
            console.log(Id, UserRights);
            UserServices.updateUser(Id, UserRights).then(res => {
                if (res.data == "success") {
                    this.loadAllUser();
                    this.setState({ modalIsOpen: false });
                    NotificationManager.success('Update user is success!', 'Message');
                }
                else
                    NotificationManager.warning('Warning message', 'Close after 3000ms', 3000);

            });
        }

    }

    loadAllUser = () => {
        UserServices.loadListUserByAdmin().then(res => {
            var listPost = res.data;
            this.setState({
                modalIsOpen: false,
                list: listPost.map((item, i) => (<tr>
                    <td key={i}>{item.Id}</td>
                    <td key={i}>{item.Username}</td>
                    <td key={i}>{item.Email}</td>
                    <td key={i}>{moment(item.Dob).format("YYYY-MM-DD")}</td>
                    <td key={i}>{item.UserRights}</td>
                    <td><button type="button" class="btn btn-info" onClick={() => this.openModal(item)} ><i class="fa fa-pencil-square-o"></i> Detail</button></td>
                    <td><button type="button" class="btn btn-danger" onClick={() => this.deleteUser(item)} ><i class="fa fa-remove"></i> Delete</button></td>
                </tr>))
            });
        });
    }

    openModal(item) {
        console.log(item);
        this.setState({ modalIsOpen: true });
        this.setState({ Id: item.Id });
        this.setState({ UserAdmin: item.UserRights });
        var arr = [];
        arr.push(<div className="modalArticle">
            <h4>Id : </h4>
            <p >{item.Id}</p>
            <h4>Username : </h4>
            <p>{item.Username}</p>
            <h4>Email : </h4>
            <p >{item.Email}</p>
            <h4>Dob : </h4>
            <p >{moment(item.Dob).format("YYYY-MM-DD")}</p>
            <h4>UserRights :</h4>
            <Select
                placeholder={item.UserRights}
                onChange={this.handleSelect}
                options={options}
                isSearchable={false}
            /><br />
        </div >)
        this.setState({
            user: arr
        });

    }

    afterOpenModal() {
        this.subtitle.style.color = '#5F9EA0';
    }

    closeModal() {
        this.setState({ modalIsOpen: false });
    }

    render() {
        const { Status } = this.state;

        return (
            <postwriter>
                <NotificationContainer />
                <h2>Users Management</h2>
                <table className="listPosts">
                    <tr>
                        <th width="30px">Id</th>
                        <th width="30px">Username</th>
                        <th>Email</th>
                        <th>Dob</th>
                        <th>UserRights</th>
                        <th width="20px">Detail</th>
                        <th width="20px">Delete</th>
                    </tr>
                    {this.state.list}
                </table>
                <div className="modalPostAdmin">
                    <Modal
                        isOpen={this.state.modalIsOpen}
                        onAfterOpen={this.afterOpenModal}
                        onRequestClose={this.closeModal}
                        style={customStyles}
                        contentLabel="Post Modal"
                    >
                        <h2 ref={subtitle => this.subtitle = subtitle}>Detailed Article (Admin)</h2>
                        {this.state.user}
                        <button type="button" class="btn btn-success" onClick={this.updateUserRights}>update</button>
                        <button type="button" class="btn btn-danger" onClick={this.closeModal}>close</button>

                    </Modal>
                </div>
            </postwriter>

        );
    };
}
export default AdminUser;