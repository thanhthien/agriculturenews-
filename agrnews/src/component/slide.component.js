import React, { Component } from 'react';
import OwlCarousel from "react-owl-carousel2";
class Slide extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			items: [
				<div key={1} class="item"> 
								<div class="article-img">
				<img src="images/agr4.jpg" alt="The Last of us" />
				</div>
				<div class="article-body">
				<ul class="article-info">
					<li class="article-category"><a href="#">News</a></li>
					<li class="article-type"><i class="fa fa-camera"></i></li>
				</ul>
				<h2 class="article-title"><a href="#">Sâu keo mùa thu đe dọa mùa màng ở châu Á.</a></h2>
				<ul class="article-meta">
					<li><i class="fa fa-clock-o"></i> January 31, 2017</li>
					<li><i class="fa fa-comments"></i> 33</li>
				</ul>
				</div>
			</div>
,				<div key={2} class="item">
				<div class="article-img">
					<img src="images/agr2.jpg" alt="GTA V" />
				</div>
				<div class="article-body">
				<ul class="article-info">
					<li class="article-category"><a href="#">News</a></li>
					<li class="article-type"><i class="fa fa-camera"></i></li>
				</ul>
				<h2 class="article-title"><a href="#">Yên Bái: Buộc hai trại cá hồi ngừng hoạt động.</a></h2>
				<ul class="article-meta">
					<li><i class="fa fa-clock-o"></i> January 31, 2017</li>
					<li><i class="fa fa-comments"></i> 33</li>
				</ul>
				</div>
				</div>,
				<div key={3} class="item">
								<div class="article-img">
					<img src="images/agr3.jpg" alt="Mirror Edge" />
					</div>
								<div class="article-body">
				<ul class="article-info">
					<li class="article-category"><a href="#">News</a></li>
					<li class="article-type"><i class="fa fa-camera"></i></li>
				</ul>
				<h2 class="article-title"><a href="#">Phát hiện nhiều ổ dịch tả lợn châu Phi tại Đồng Tháp.</a></h2>
				<ul class="article-meta">
					<li><i class="fa fa-clock-o"></i> January 31, 2017</li>
					<li><i class="fa fa-comments"></i> 33</li>
				</ul>
				</div>
				</div>,
			],

			itemNo: 2,
			loop: true,
			nav: true,
			navText:["<i class ='fa fa-angle-left'></i>","<i class ='fa fa-angle-right'></i>"],
			rewind: true,
			autoplay: true
		};
	}

	startPlay() {
		this.setState({
			autoplay: true
		});
	}

	stopPlay() {
		this.setState({
			autoplay: false
		});
	}

	addItem() {
		let items = this.state.items;
		let newItem = <div key={this.state.items.length + 1} class="item"><img src="/img/fullimage2.jpg" alt="GTA V" /></div>;
		items.push(newItem);
		this.setState({ items });
	}

	newOptions() {
		this.setState({
			nav: true // Show next and prev buttons,
		});
	}

	render() {
		const options = {
			items: this.state.itemNo,
			loop: this.state.loop,
			nav: this.state.nav,
			navText:["<i class ='fa fa-angle-left'></i>","<i class ='fa fa-angle-right'></i>"],
			rewind: this.state.rewind,
			autoplay: this.state.autoplay
		};

		const events = {
			onDragged: function(event) {
				//  console.log('onDragged: ' + event.type); 
				},
			onChanged: function(event) {
				//  console.log('onChanged: ' + event.type); 
				},
			onTranslate: function(event) {
				//  console.log('onTranslate: ' + event.type); 
				}
		};

		return (
			<div>
				<OwlCarousel
					ref="car"
					options={options}
					events={events}
				>
					{this.state.items}
				</OwlCarousel>

				{/* <button onClick={() => this.refs.car.prev()}>
					prev
				</button> */}
			</div>
		);
	}
}
export default Slide