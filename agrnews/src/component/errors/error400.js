import React, { Component } from 'react';
import '../../assets/css/style404.css'
class Error extends Component {
    render() {
        return (
            <div id="notfound">
            <div class="notfound">
                <div class="notfound-404">
                    <h1>404</h1>
                    <h2>Page not found</h2>
                </div>
                <a href="http://127.0.0.1:3000/">Homepage</a>
            </div>
        </div>

        );
    };
}
export default Error;