import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import PrivateLayout from './route/privatelayout'
import PublicLayout from './route/publiclayout'
import PageError from './assets/css/style404.css'

const App = () => (
  <div className="app">
    <BrowserRouter>
      <Switch>
        <Route exact path='/404' component={PrivateLayout} /> 
        <Route exact path="/reset/" component={PrivateLayout} />
        <Route exact path="/login/" component={PrivateLayout} />
        <Route exact path="/register/" component={PrivateLayout} />
        <Route path="/" component={PublicLayout} />
      </Switch>
    </BrowserRouter>
  </div>
);


export default App
