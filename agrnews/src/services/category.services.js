import axios from 'axios';
import * as constants from "../common/constants"

const categoryServices = {

    getAll: function () {
        var url = constants.API_BASE_URL + "/categories/";
        return axios.get(url).then(response => {
            console.log(response)
            return response;
        }).catch(err => {
            return err.response;
        });
    },

    addCat: function (CatName, Url, ParentIDS) {
        console.log(CatName, Url, ParentIDS);
        var url = constants.API_BASE_URL + "/categories/addcat";
        return axios.post(url, {
            CatName: CatName,
            Url: Url,
            ParentIDS: ParentIDS,
        }).then(res => {
            return res;
        }).catch(err => {
            return err.response;
        });
    },



    loadListCatByAdmin: function () {
        var url = constants.API_BASE_URL + "/categories/admin/all";
        return axios.get(url, {
        }).then(res => {
            return res;
        }).catch(err => {
            return err.response;
        });
    },

    updateCat: function (CatID, CatName, Url, ParentIDS) {
        var url = constants.API_BASE_URL + "/categories/admin/cat";
        return axios.post(url, {
            CatID: CatID,
            CatName: CatName,
            Url: Url,
            ParentIDS: ParentIDS,
        }).then(res => {
            return res;
        }).catch(err => {
            return err.response;
        });
    },

    deleteCat: function (CatID) {
        var url = constants.API_BASE_URL + "/categories/admin/delete";
        return axios.post(url, {
            CatID: CatID,
        }).then(res => {
            return res;
        }).catch(err => {
            return err.response;
        });
    },


}

export default categoryServices;

