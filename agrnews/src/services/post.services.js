import axios from 'axios';
import * as constants from "../common/constants"

const postServices = {

    getAll: function () {
        var url = constants.API_BASE_URL + "/posts/";
        return axios.get(url).then(response => {
            return response;
        }).catch(err => {
            return err.response;
        });
    },


    getAllNews: function () {
        var url = constants.API_BASE_URL + "/posts/10news";
        return axios.get(url).then(response => {
            return response;
        }).catch(err => {
            return err.response;
        });
    },

    getAllViews: function () {
        var url = constants.API_BASE_URL + "/posts/10views";
        return axios.get(url).then(response => {
            console.log(response)
            return response;
        }).catch(err => {
            return err.response;
        });
    },


    getAllDataPost: function (data) {
        var url = constants.API_BASE_URL + "/posts/editor/all";
        return axios.post(url, {
            PostID: data,
        }).then(res => {
            console.log(res)
            return res;
        }).catch(err => {
            return err.response;
        });
    },


    updateViews: function (data) {
        var views = (data.views + 1),
            PostID = data.PostID;
        var url = constants.API_BASE_URL + "/posts/update/views";
        return axios.post(url, {
            PostID: PostID,
            views: views
        }).then(res => {
            return res;
        }).catch(err => {
            return err.response;
        });
    },

    postsSameCategory: function (data) {
        var PostID = data.PostID,
            CatID = data.CatID;
        var url = constants.API_BASE_URL + "/posts/sameCat";
        return axios.post(url, {
            PostID: PostID,
            CatID: CatID
        }).then(res => {
            return res;
        }).catch(err => {
            return err.response;
        });
    },





    getListPost: function () {
        var categoryUrl = window.location.pathname.replace("/category/", "")
        if (categoryUrl !== undefined && categoryUrl.trim() !== "") {
            var url = constants.API_BASE_URL + "/posts/get";
            return axios.post(url, {
                url: categoryUrl
            }).then(response => {
                return response;
            }).catch(err => {
                return err.response;
            });

        }
    },

    getDetailPost: function () {
        var getPost = window.location.pathname.replace("/post/", "")
        if (getPost !== undefined && getPost.trim() !== "") {
            var url = constants.API_BASE_URL + "/posts/getpost";
            return axios.post(url, {
                Url: getPost
            }).then(response => {
                return response;
            }).catch(err => {
                return err.response;
            });

        }
    },

    SearchData: function (data) {
        var url = constants.API_BASE_URL + "/posts/search";
        return axios.post(url, {
            Search: data
        }).then(response => {
            console.log(response);
            return response;
        }).catch(err => {
            return err.response;
        });
    },


    getListPostWithPage: function (pageIndex, pageSize) {
        var categoryUrl = window.location.pathname.replace("/category/", "")
        if (categoryUrl !== undefined && categoryUrl.trim() !== "") {
            var url = constants.API_BASE_URL + "/posts/page";
            return axios.post(url, {
                url: categoryUrl,
                pageIndex: pageIndex,
                pageSize: pageSize,
            }).then(response => {
                return response;
            }).catch(err => {
                return err.response;
            });

        }
    },

    pushImage: function (data) {
        var url = constants.API_BASE_URL + "/posts/uploadImage";
        return axios.post(url, data, { // receive two parameter endpoint url ,form data 
        })
            .then(res => { // then print response status
                console.log(res);
                return res;
            })

    },


    loadListPost: function (data) {
        var url = constants.API_BASE_URL + "/posts/list";
        return axios.post(url, {
            UserID: data,
        }).then(res => {
            console.log(res)
            return res;
        }).catch(err => {
            return err.response;
        });
    },

    loadListPostEditor: function (data) {
        var url = constants.API_BASE_URL + "/posts/editor/list";
        return axios.post(url, {
            UserID: data,
        }).then(res => {
            console.log(res)
            return res;
        }).catch(err => {
            return err.response;
        });
    },

    UpdateStatusPost: function (PostID, Status) {
        var url = constants.API_BASE_URL + "/posts/edit/status";
        return axios.post(url, {
            PostID: PostID,
            Status: Status
        }).then(res => {
            return res;
        }).catch(err => {
            return err.response;
        });
    },

    loadListPostAdmin: function (data) {
        var url = constants.API_BASE_URL + "/posts/admin/list";
        return axios.post(url, {
            UserID: data,
        }).then(res => {
            console.log(res)
            return res;
        }).catch(err => {
            return err.response;
        });
    },
}

export default postServices;

