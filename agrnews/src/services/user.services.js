import axios from 'axios';
import * as constants from "../common/constants"


const userServices = {

    getFormRegister: function (obj) {
        var headers = new Headers();
        console.log(obj)
        const data = JSON.stringify({
            "Username": obj.name.value, "Email": obj.email.value,
            "Dob": obj.dob.value, "pass": obj.password.value
        });
        headers.append("Content-type", "application/json");
        return fetch(constants.API_BASE_URL + "/users/register", {
            method: "POST",
            headers: {
                'Content-type': 'application/json'
            },
            body: data
        })
            .then(res => {
                return res.status
            })
            .catch(error => {
                return error;
            });
    },

    checkNameValue: function (userName, userEmaill) {
        var url = constants.API_BASE_URL + "/users/available";
        return axios.post(url, {
            name: userName,
            email: userEmaill
        }).then(Response => {
            console.log(Response)
            return Response.data
        })
    },

    checkLoginUser: function (user_Name, user_Password) {
        console.log(user_Name, user_Password);
        var url = constants.API_BASE_URL + "/users/login";
        return axios.post(url, {
            username: user_Name,
            password: user_Password
        }).then(res => {
            var user = res.data;
            if (user) {
            console.log(user);
            console.log("22222222222222222");
                user.authdata = window.btoa(user_Name + ':' + user_Password);
                localStorage.setItem('user', JSON.stringify(user));
            }
            return user;
        })
    },

    logout: function () {
        localStorage.removeItem('user');
        console.log("Logout success!")
    },

    changePassword: function (id, username, password, newPasword) {
        var url = constants.API_BASE_URL + "/users/changepass";
        return axios.post(url, {
            id: id,
            username: username,
            password: password,
            newPasword: newPasword
        }).then(res => {
            return res.data;
        })
    }, 

    resetEmail: function (email) {
        var url = constants.API_BASE_URL + "/users/nodemailer";
        return axios.post(url, {
            Email: email,
        }).then(res => {
            console.log(res);
            return res.data;
        })
    }, 

    loadListUserByAdmin: function (data) {
        var url = constants.API_BASE_URL + "/users/admin/all";
        return axios.post(url, {
        }).then(res => {
            console.log(res)
            return res;
        }).catch(err => {
            return err.response;
        });
    },


    updateUser: function (Id, UserRights) {
        var url = constants.API_BASE_URL + "/users/admin/user";
        return axios.post(url, {
            Id: Id,
            UserRights: UserRights,
        }).then(res => {
            console.log(res)
            return res;
        }).catch(err => {
            return err.response;
        });
    },
 
    deleteUserServices: function (Id) {
        var url = constants.API_BASE_URL + "/users/admin/user/delete";
        return axios.post(url, {
            Id: Id,
        }).then(res => {
            console.log(res)
            return res;
        }).catch(err => {
            return err.response;
        });
    },
    
}

export default userServices;
