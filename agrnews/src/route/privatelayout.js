import React, { Component } from 'react'
import { Route, BrowserRouter as Router, Switch, Redirect } from 'react-router-dom'
import Register from '../component/User/register.component';
import Login from '../component/User/login.component';
import ResetPass from '../component/User/resetpass.component';
import My404Component from "../component/errors/error400";

class PrivateLayout extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route exact path="/register" component={Register} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/reset" component={ResetPass} />
          <Route exact path='/404' component={My404Component} />

        </Switch>
      </div>
    );
  }
}
export default PrivateLayout;