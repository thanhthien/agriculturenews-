import React, { Component } from 'react'
import { Route, BrowserRouter as Rouzter, Switch, Redirect } from 'react-router-dom'
import Header from '../component/header.component';
import Footer from '../component/footer.component';
import Home from '../component/home';
import ListPost from '../component/categories';
import Account from '../component/User/account/account.component';
import ChangePass from '../component/User/account/changePass.component';
import Writer from '../component/writer';
import ListPostWriter from '../component/listPostWriter';
import ListPostEdirtor from '../component/editor';
import AdminEdirtor from '../component/adminPost';
import AdminUsers from '../component/adminUser';
import AdminCat from '../component/adminCat';
import Post from '../component/posts';
import Search from '../component/search';
import SearchResult from '../component/searchResult';
import My404Component from '../component/errors/error400';
import addcat from '../component/addCat';
import pagination from '../component/pagination';





import "../assets/css/bootstrap.min.css";
import "../assets/css/font-awesome.min.css";
import "../assets/css/owl.carousel.css";
import "../assets/css/owl.theme.default.css";
import "../assets/css/style.css";
import "../assets/css/owl.video.play.png";
export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    localStorage.getItem('user')
      ? <Component {...props} />
      : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
  )} />
)

class PublicLayout extends Component {
  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route   path="/searchresult" component={SearchResult} />
          <Route   path="/pagination" component={pagination} />
          <Route exact path="/" exact component={Home} />
          <Route exact path="/search" component={Search} />
          <Route exact path="/category/:categoryUrl/" component={ListPost} />
          <PrivateRoute exact path="/writer" component={Writer} />
          <PrivateRoute exact path="/postswriter" component={ListPostWriter} />
          <PrivateRoute exact path="/addCat" component={addcat} />
          <PrivateRoute exact path="/post/:postUrl/" component={Post} />
          <PrivateRoute exact path="/postseditor" component={ListPostEdirtor} />
          <PrivateRoute exact path="/admin/post" component={AdminEdirtor} />
          <PrivateRoute exact path="/admin/uses" component={AdminUsers} />
          <PrivateRoute exact path="/admin/category" component={AdminCat} />
          <PrivateRoute exact path="/account" component={Account} />
          <PrivateRoute exact path="/changepassword" component={ChangePass} />
          <Route  exact path='/404' component={My404Component} />
          <Redirect from='*' to='/404' />
        </Switch>
        <Footer />
      </div>
    );
  }
}
export default PublicLayout;